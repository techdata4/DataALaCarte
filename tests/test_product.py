import pytest
from uuid import uuid4
from unittest.mock import MagicMock, patch
from fastapi import HTTPException
from sqlalchemy.exc import SQLAlchemyError
from sqlalchemy.orm import Session
from app.routes.order import add_product_to_order
from app.routes.product import create_product
from app.schemas.product_schema import ProductCreate
from fastapi import HTTPException


@pytest.fixture
def order_id() :
    return uuid4()


@pytest.fixture
def product_id() :
    return uuid4()


# Fixture pour simuler une session de base de données
@pytest.fixture
def mock_db_session() :

    session_mock = MagicMock(spec = Session)


    yield session_mock



@pytest.mark.asyncio
async def test_create_product_success(mock_db_session):
    product_data = ProductCreate(
        name="bidule",
        description="Test Description",
        image = "test_image_url",
        price = 10.99,
        available = True


    )
    response = await create_product(product_data, mock_db_session)

    assert response.name == product_data.name
    assert response.description == product_data.description
    assert response.image == product_data.image
    assert response.price == product_data.price
    assert response.available == product_data.available



@pytest.mark.asyncio
async def test_add_product_to_order_no_order(mock_db_session, order_id, product_id) :
    mock_db_session.query().filter().first.side_effect = [None]
    with pytest.raises(HTTPException) as excinfo :
        await add_product_to_order(order_id, product_id, 10, mock_db_session)
    assert excinfo.value.status_code == 404
    assert "Commande non trouvée" in str(excinfo.value)


@pytest.mark.asyncio
async def test_add_product_to_order_no_product(mock_db_session, order_id, product_id) :
    order_mock = MagicMock()
    mock_db_session.query().filter().first.side_effect = [order_mock, None]
    with pytest.raises(HTTPException) as excinfo :
        await add_product_to_order(order_id, product_id, 10, mock_db_session)
    assert excinfo.value.status_code == 404
    assert "Produit non trouvé" in str(excinfo.value)


@pytest.mark.asyncio
async def test_add_product_to_order_existing_order_line(mock_db_session, order_id, product_id) :
    order_mock = MagicMock()
    product_mock = MagicMock()
    mock_db_session.query().filter().first.side_effect = [order_mock, product_mock]
    mock_db_session.execute().scalar.return_value = 5

    response = await add_product_to_order(order_id, product_id, 10, mock_db_session)
    assert response == {"message" : f"Produit {product_id} ajouté à la commande {order_id}"}



