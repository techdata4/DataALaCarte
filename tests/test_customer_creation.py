import unittest
from unittest.mock import Mock
from faker import Faker
import uuid
from app.models.customer import Customer
from app.models.address import Address
from app.routes.customer import create_customer, create_db_session
from app.schemas.customerSchema import CustomerCreate, AddressCreate


class TestCreateCustomer(unittest.TestCase) :

    def setUp(self) :
        self.faker = Faker()
        self.db = Mock()
        self.db.commit = Mock()
        self.db.refresh = Mock()

    def test_create_customer(self) :
        address_data = AddressCreate(
            id = str(uuid.uuid4()),
            street = self.faker.street_name(),
            city = self.faker.city(),
            state = self.faker.state(),
            zipcode = self.faker.zipcode(),
            country = self.faker.country()
        )
        customer_data = CustomerCreate(
            street = self.faker.street_name(),
            city = self.faker.city(),
            state = self.faker.state(),
            zipcode = self.faker.zipcode(),
            country = self.faker.country()
        )

        result = create_customer(customer_data, self.db)

        self.db.add.assert_called()  # Ensure the add method was called
        self.db.commit.assert_called()  # Ensure the commit method was called
        self.db.refresh.assert_called()  # Ensure the refresh method was called

        self.assertIsInstance(result, Customer)
        self.assertEqual(result.email, customer_data.email)
        self.assertEqual(result.name, customer_data.name)
        self.assertEqual(result.phone, customer_data.phone)
        self.assertEqual(result.address.street, address_data.street)
        self.assertEqual(result.address.city, address_data.city)
        self.assertEqual(result.address.state, address_data.state)
        self.assertEqual(result.address.zipcode, address_data.zipcode)
        self.assertEqual(result.address.country, address_data.country)


if __name__ == '__main__' :
    unittest.main()