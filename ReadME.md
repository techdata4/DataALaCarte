
## Projet "Data à la carte" - README

### Description du projet

Le projet "Data à la carte" est une application FastAPI conçue pour gérer et manipuler des données de manière efficace.
Cette application modulaire facilite la gestion des clients et des informations associées grâce à une architecture
claire et des technologies de pointe.

### Structure du projet

```
├── Http
│   ├── CreateAddress.http
│   ├── CreateCustormer.http
│   ├── CreateProducts.http
│   ├── GetAllProducts.http
│   └── GetProductByID.http
├── LisezMoi
│   ├── CreateCustomer_DTOScham.png
│   ├── MCD.png
│   ├── Model.md
│   ├── Model.puml
│   ├── Pydentic_Schema
│   │   ├── Customer.puml
│   │   ├── Product.puml
│   │   └── PydenticEntities.puml
│   └── bdd-sell-6564b1fd665a7951284973.png
├── ReadME.md
├── app
│   ├── config
│   │   └── exception.py
│   ├── dependencies
│   │   └── database.py
│   ├── initialize_db.py
│   ├── main.py
│   ├── models
│   │   ├── address.py
│   │   ├── association.py
│   │   ├── customer.py
│   │   ├── order.py
│   │   └── product.py
│   ├── openapi.json
│   ├── routes
│   │   ├── address.py
│   │   ├── customer.py
│   │   ├── order.py
│   │   ├── product.py
│   │   └── root.py
│   └── schemas
│       ├── address_schema.py
│       ├── customerSchema.py
│       ├── orderSchema.py
│       └── product_schema.py
└── tests
    ├── test_customer_creation.py
    └── test_root.py

   
   ```

### Objectif du projet

L'objectif principal de "Data à la carte" est de fournir une interface API pour la gestion des données clients, en
utilisant FastAPI .

### Modules du projet

- `app`: Point d'entrée de l'application et fichiers de configuration.
- `dependencies`: Gestion des dépendances, y compris la base de données.
- `models`: Modèles de données pour structurer les informations.
- `routes`: Définition des routes API pour l'interaction avec l'application.
- `schemas`: Schémas de validation des données.

### Installation et Configuration

#### Prérequis

- Python 3.11
- Pipenv
- FastAPI
- Uvicorn
- Pytest
- SQLAlchemy

#### Installation

1. Cloner le dépôt : `git clone [url_du_dépôt]`
2. Installer les dépendances : `pipenv install`
3. Activer l'environnement virtuel : `pipenv shell`

### Utilisation

Pour lancer l'application, exécutez `uvicorn app.main:app --reload`. L'API sera accessible à l'
adresse `http://localhost:8000`.

### Tests

Pour exécuter les tests, utilisez la commande `pytest` dans le répertoi