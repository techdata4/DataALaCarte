# Description du Modèle de Données à la Carte

## RÉSUMÉ :

Ce modèle de données offre une vision structurée et détaillée des informations essentielles à la gestion des ventes de
notre entreprise. Il articule les interactions entre clients, commandes et produits, tout en garantissant une gestion
optimale des adresses pour une expérience client personnalisée et efficace.

## TABLES PRINCIPALES :

- **Client** : Chaque entrée de cette table représente un client unique, avec des informations telles que l'email, le
  nom, l'identifiant unique, le numéro de téléphone et l'adresse par défaut(). Cette table est le cœur de la gestion
  clientèle, permettant un suivi individualisé et des analyses comportementales précises.

- **Order** : La table des commandes enregistre chaque transaction effectuée par les clients. Les détails inclus sont la
  référence client, les lignes de commandes, l'adresse de livraison, l'adresse de facturation et la méthode de paiement
  utilisée. Elle joue un rôle central dans l'analyse des tendances de vente et la logistique.

- **Product** : Cette table liste tous les produits proposés par l'entreprise, en détaillant le nom, la description, les
  images, le prix et la disponibilité. Sa gestion impacte directement le catalogue de produits et le suivi des stocks.

- **Adresse** : Contenant les détails géographiques tels que la rue, la ville, le code postal, l'état et le pays, la
  table des adresses est vitale pour l'expédition et l'analyse géographique des données de vente.

- **client_adress** : En tant que table associative, elle fait le lien entre les clients et leurs adresses, permettant
  de gérer plusieurs adresses pour un seul client, ce qui est crucial pour les différentes options de livraison et de
  facturation.

## RELATIONS ENTRE LES TABLES :

- La relation entre les tables **Client** et **Order** est de type "un à plusieurs", un client pouvant effectuer
  plusieurs commandes.
- La table **Order_line** sert de "pont" entre les commandes et les produits, reflétant une relation "plusieurs à
  plusieurs" où chaque commande peut contenir plusireurs produits.
- Les clients sont liés aux adresses par une relation "plusieurs à plusieurs" via la table **client_address**,
  permettant
  aux clients d'avoir plusieurs adresses enregistrées.

## ENUMÉRATIONS UTILISÉES :

- **Payment_method** : Cette énumération définit les différentes méthodes de paiement acceptées, telles que la carte de
  crédit, le chèque bancaire à la livraison et le paiement en espèces à la livraison.
- **Order_state** : Elle décrit les différents états que peut prendre une commande : en panier, validée, envoyée et
  livrée.

## CONTRAINTE D'INTEGRITEES :

Bien qu'il ne s'agisse pas d'un model de données relationnel au sens propre, les contraintes d'intégrites pourront etre
défini comme tel :

### Contraintes D'UNICITE :

- le champ email doit être la clef primaire de la table **Client**.
- ( **Attention** :
    - le champ email doit être unique
    - un client doit avoir au minimum une adresse mail qui permet de l'identifier. Il est donc impossible d'avoir un
      client sans adresse mail.
- le champ id doit être la clef primaire de la table **Order**.
-
- Le `email` dans la table **Client** doit être unique pour chaque client.
- L'`id` dans les tables **Client**, **Order**, **Product** et **Adresse** sert d'identifiant unique et ne peut se
  répéter.

### Contraintes de Domaine :

- Le `price` dans la table **Product** doit être un nombre positif.
- L'`id` de client dans la table **Order** doit correspondre à un `id` existant dans la table **Client**.
- Les champs `delivery_address` et `billing_address` dans la table **Order** doivent correspondre à des `id` existants
  dans la table **Adresse**.
- Les `id` de `client` et `adress` dans la table **client_adress** doivent correspondre respectivement à des `id`
  existants dans les tables **Client** et **Adresse**.
- le `order_id` dans la table **Order_line** doit correspondre à un `id` existant dans la table **Order**.

### Contraintes de Clé Étrangère :

- Les `client_id` dans les tables **Order** et **client_adress** doivent faire référence à un `id` valide dans la table
  **Client**.
- Les `order_id` et `product_id` dans la table **Order_line** doivent correspondre à des `id` valides dans les tables *
  *Order** et **Product** respectivement.
- Les `adress_id` dans la table **client_adress** doivent correspondre à des `id` valides dans la table **Adresse**.
  