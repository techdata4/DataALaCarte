import uuid
from sqlalchemy import Column, String, ForeignKey, Boolean, DateTime
from sqlalchemy.orm import relationship
from app.dependencies.database import Base
from .association import Customer_address_association
from .order import Order


class Customer(Base):
    __tablename__ = 'customer'
    id = Column(
        String(36),
        primary_key=True,
        default=lambda: str(
            uuid.uuid4()))
    email = Column(String(255), nullable=False, unique=True)
    name = Column(String(100), nullable=False)
    phone = Column(String(20), nullable=True)
    default_address_id = Column(
        String(36),
        ForeignKey('address.id'),
        nullable=True)
    is_active = Column(Boolean, default=True)
    deleted_at = Column(DateTime, nullable=True)

    # In your Customer class
    orders = relationship("Order", back_populates="customer")
    addresses = relationship(
        "Address",
        secondary=Customer_address_association,
        back_populates="customers")
