from sqlalchemy import Table, Column, String, ForeignKey, Integer
from app.dependencies.database import Base

Customer_address_association = Table(
    'CustomerAddress',
    Base.metadata,
    Column(
        'customer_id',
        String(36),
        ForeignKey('customer.id'),
        primary_key=True),
    Column(
        'address_id',
        String(36),
        ForeignKey('address.id'),
        primary_key=True))

Order_line_association = Table(
    'order_line',
    Base.metadata,
    Column('order_id', String(36), ForeignKey('order.id'), primary_key=True),
    Column('product_id', String(36), ForeignKey('product.id'), primary_key=True),
    Column('quantity', Integer, nullable=False)
)
