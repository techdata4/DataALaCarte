import uuid

from sqlalchemy import Column, String
from sqlalchemy.orm import relationship

from app.dependencies.database import Base
from .association import Customer_address_association


class Address(Base):
    __tablename__ = 'address'

    id = Column(
        String(36),
        primary_key=True,
        default=lambda: str(
            uuid.uuid4()))
    street = Column(String(255), nullable=False)
    city = Column(String(100), nullable=False)
    state = Column(String(100), nullable=False)
    zipcode = Column(String(20), nullable=False)
    country = Column(String(100), nullable=False)

    customers = relationship(
        "Customer",
        secondary=Customer_address_association,
        back_populates="addresses")
