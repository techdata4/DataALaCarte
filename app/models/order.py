import uuid
from sqlalchemy import Column, String, DateTime, ForeignKey, Enum as SQLAlchemyEnum
from sqlalchemy.orm import relationship
from app.dependencies.database import Base
from app.models.association import Order_line_association
from app.schemas.orderSchema import OrderStateEnum


class Order(Base):
    __tablename__ = 'order'
    id = Column(
        String(36),
        primary_key=True,
        default=lambda: str(
            uuid.uuid4()))
    customer_id = Column(String(36), ForeignKey('customer.id'), nullable=True)
    date_order = Column(DateTime, nullable=True)
    delivery_address_id = Column(
        String(36),
        ForeignKey('address.id'),
        nullable=True)
    billing_address_id = Column(
        String(36),
        ForeignKey('address.id'),
        nullable=True)

    payment_method = Column(String(36), nullable=True)
    status = Column(SQLAlchemyEnum(OrderStateEnum), nullable=False)

    # Relations
    customer = relationship("Customer", back_populates="orders")
    products = relationship(
        "Product",
        secondary=Order_line_association,
        back_populates="orders")
