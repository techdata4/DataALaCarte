import uuid
from sqlalchemy import Column, String, Text, Numeric, Boolean, DateTime
from sqlalchemy.orm import relationship
from app.dependencies.database import Base
from app.models.association import Order_line_association
from app.models.order import Order


class Product(Base):
    __tablename__ = 'product'
    id = Column(
        String(36),
        primary_key=True,
        default=lambda: str(
            uuid.uuid4()))
    name = Column(String(100), nullable=False)
    description = Column(Text, nullable=True)
    image = Column(String(255), nullable=True)
    price = Column(Numeric(10, 2), nullable=False)
    available = Column(Boolean, nullable=False, default=True)
    is_active = Column(Boolean, default=True)
    deleted_at = Column(DateTime, nullable=True)



    # Relation avec Order via la table associative OrderLine
    orders = relationship(
        Order,
        secondary=Order_line_association,
        back_populates="products")

    def __str__(self):
        return f"Product(id={self.id}, name={self.name}, price={self.price}, available={self.available})"
