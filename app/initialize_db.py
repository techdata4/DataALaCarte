from app.models.address import Address
from app.models.order import Order
from app.models.product import Product
from app.models.customer import Customer
from app.models.association import Customer_address_association, Order_line_association

from app.dependencies.database import Base
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
import os
from dotenv import load_dotenv

load_dotenv(dotenv_path=".env", verbose=True, )

# Configuration de base de données
SQLALCHEMY_DATABASE_URL = os.getenv("DATABASE_URL")

if SQLALCHEMY_DATABASE_URL is None:
    raise ValueError(
        "La variable d'environnement 'DATABASE_URL' n'est pas définie.")

# Création de l'engine pour la base de données
engine = create_engine(SQLALCHEMY_DATABASE_URL, echo=True)

# Configuration de la Session
SessionLocal = sessionmaker(autocommit=False, autoflush=False, bind=engine)


def create_tables():
    Base.metadata.create_all(bind=engine)


if __name__ == "__main__":
    create_tables()
