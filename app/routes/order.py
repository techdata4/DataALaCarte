from datetime import datetime
from typing import List

from fastapi import APIRouter, Depends, HTTPException, status, Path
from sqlalchemy import select, and_, exists
from sqlalchemy.orm import Session, joinedload
from sqlalchemy.exc import SQLAlchemyError
from uuid import UUID
from app.models.address import Address
from app.models.order import Order
from app.dependencies.database import SessionLocal
from app.schemas.orderSchema import OrderStateEnum, OrderResponse
from app.models.product import Product
from app.models.association import Order_line_association, Customer_address_association

router = APIRouter()


def create_db_session() :
    db = SessionLocal()
    try :
        yield db
    finally :
        db.close()


@router.put("/order",
            status_code = status.HTTP_201_CREATED,
            summary = "Créer une Nouvelle Commande",
            description = "Crée une nouvelle commande avec un état initial 'basket' et retourne son ID.",
            response_description = "ID de la commande créée")
async def create_order(db: Session = Depends(create_db_session)) :
    """
    Crée une nouvelle commande avec un état initial 'basket' et retourne son ID.
    """
    new_order = Order(
        date_order = datetime.now(),
        status = OrderStateEnum.basket.value)
    db.add(new_order)
    try :
        db.commit()
        db.refresh(new_order)
    except SQLAlchemyError as e :
        db.rollback()
        raise HTTPException(status_code = 400, detail = f"Database error: {e}")
    finally :
        db.close()
    return {"order_id" : str(new_order.id)}


@router.put("/order/{order_id}/{product_id}/{quantity}",
            status_code = status.HTTP_200_OK,
            summary = "Ajouter un Produit à une Commande",
            description = "Ajoute un certain nombre d'un produit spécifique à une commande spécifique.",
            response_description = "Confirmation de l'ajout du produit à la commande")
async def add_product_to_order(
        order_id: UUID = Path(...,
                              description = "L'identifiant unique de la commande"),
        product_id: UUID = Path(...,
                                description = "L'identifiant unique du produit"),
        quantity: int = Path(...,
                             description = "La quantité du produit à ajouter à la commande"),
        db: Session = Depends(create_db_session)) :
    """
    Ajoute un produit à une commande.

    - **order_id**: L'identifiant unique de la commande.
    - **product_id**: L'identifiant unique du produit.
    - **quantity**: La quantité du produit à ajouter à la commande.
    """
    order_id_str = str(order_id)
    product_id_str = str(product_id)
    order = db.query(Order).filter(Order.id == order_id_str).first()
    if not order :
        raise HTTPException(status_code = 404, detail = "Commande non trouvée")

    product = db.query(Product).filter(Product.id == product_id_str).first()
    if not product :
        raise HTTPException(status_code = 404, detail = "Produit non trouvé")

    order_line = db.query(Order_line_association).filter(
        and_(
            (Order_line_association.c.order_id == order_id_str),
            (Order_line_association.c.product_id == product_id_str))).first()

    if order_line is not None :
        db.execute(
            Order_line_association.update()
            .where(
                and_(
                    Order_line_association.c.order_id == order_id_str,
                    Order_line_association.c.product_id == product_id_str
                )
            )
            .values(quantity = quantity)
        )
    else :
        db.execute(
            Order_line_association.insert().values(
                order_id = order_id_str,
                product_id = product_id_str,
                quantity = quantity))

    try :
        db.commit()
    except SQLAlchemyError as e :
        db.rollback()
        raise HTTPException(status_code = 400, detail = f"Database error: {e}")
    finally :
        db.close()

    return {"message" : f"Produit {product_id} ajouté/actualisé dans la commande {order_id} avec succès"}


# Get all orders
@router.get("/orders", response_model = List[OrderResponse])
async def get_all_orders(db: Session = Depends(create_db_session)) :
    try :
        orders = db.query(Order).options(joinedload(Order.products)).all()
        response = []
        for order in orders :
            order_lines = []
            for assoc in order.products :
                order_line = {
                    "product_id" : assoc.product_id,
                    "quantity" : assoc.quantity
                }
                order_lines.append(order_line)

            order_data = {
                "id" : order.id,
                "customer_id" : order.customer_id,
                "order_date" : order.date_order,
                "delivery_address_id" : order.delivery_address_id,
                "billing_address_id" : order.billing_address_id,
                "payment_method" : order.payment_method,
                "status" : order.status,
                "order_lines" : order_lines
            }
            response.append(OrderResponse(**order_data))
        return response
    except Exception as e :
        raise HTTPException(status_code = 500, detail = str(e))


@router.put("/order/{order_id}/delivery/{address_id}",
            status_code = status.HTTP_200_OK,
            summary = "Associer une Adresse de Livraison à une Commande",
            description = "Associe une adresse de livraison à une commande. "
                          "Si un client est associé à la commande et que "
                          "l'adresse n'appartient pas au client, renvoie une erreur 400.",
            response_description = "Confirmation de l'association de l'adresse.")
async def assign_delivery_address_to_order(
        order_id: UUID = Path(...,
                              description = "L'identifiant unique de la commande"),
        address_id: UUID = Path(...,
                                description = "L'identifiant unique de la commande"),
        db: Session = Depends(create_db_session)) :
    """
    Associe une adresse de livraison à une commande.

    Args:
        order_id (UUID): L'identifiant unique de la commande.
        address_id (UUID): L'identifiant unique de l'adresse de livraison.

    Returns:
        dict: Un message confirmant l'association de l'adresse.
    """
    # Convert UUID to string for database query
    order_id_str = str(order_id)
    address_id_str = str(address_id)

    order = db.query(Order).filter(Order.id == order_id_str).first()
    if not order :
        raise HTTPException(status_code = 404, detail = "Commande non trouvée")

    address = db.query(Address).filter(Address.id == address_id_str).first()
    if not address :
        raise HTTPException(status_code = 404, detail = "Adresse non trouvée")

    if order.customer_id :
        address_belongs_to_customer = db.query(exists().where(
            and_(Customer_address_association.c.customer_id == str(order.customer_id),
                 Customer_address_association.c.address_id == address_id_str)
        )).scalar()
        if not address_belongs_to_customer :
            raise HTTPException(
                status_code = 400,
                detail = "L'adresse n'appartient pas au client de la commande")

    order.delivery_address_id = address_id_str
    db.commit()
    return {"message" : "Adresse de livraison associée à la commande avec succès"}
