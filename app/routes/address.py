from fastapi import APIRouter, Depends, HTTPException, status
from sqlalchemy.orm import Session
from app.models.address import Address
from app.models.association import Customer_address_association
from app.schemas.address_schema import AddressCreate, AddressResponse
from app.dependencies.database import SessionLocal

router = APIRouter()


def create_db_session():
    db = SessionLocal()
    try:
        yield db
    finally:
        db.close()


@router.post("/address", response_model=AddressResponse,
             status_code=status.HTTP_201_CREATED)
async def create_address(address_data: AddressCreate, db: Session = Depends(create_db_session)):
    """
    Crée une nouvelle adresse ou retourne l'ID si elle existe déjà.

    - **address_data**: Le corps de la requête contenant les détails de l'adresse.
    - **Retourne**: L'objet AddressResponse de l'adresse créée ou existante.
    """
    existing_address = db.query(Address).filter_by(
        **address_data.model_dump()
    ).first()

    if existing_address:
        return existing_address

    new_address = Address(**address_data.model_dump())
    db.add(new_address)
    try:
        db.commit()
        db.refresh(new_address)
    except Exception as e:
        db.rollback()
        raise HTTPException(
            status_code=status.HTTP_500_INTERNAL_SERVER_ERROR,
            detail=str(e))
    return new_address
