from datetime import datetime
from typing import List
from uuid import UUID

from fastapi import APIRouter, Depends, status, HTTPException, Path, Body
from sqlalchemy.orm import Session
from sqlalchemy.exc import SQLAlchemyError

from app.config.exception import handle_db_error
from app.dependencies.database import SessionLocal
from app.models.address import Address
from app.models.association import Customer_address_association
from app.models.customer import Customer
from app.models.order import Order
from app.schemas.address_schema import AddressResponse
from app.schemas.customerSchema import CustomerCreate, CustomerResponse, CustomerUpdate

router = APIRouter()


def create_db_session():
    db = SessionLocal()
    try:
        yield db
    finally:
        db.close()


@router.post("/api/customers/",
             response_model=CustomerResponse,
             status_code=status.HTTP_201_CREATED,
             summary="Créer un Nouveau Client",
             description="Crée un nouveau client avec les adresses fournies et retourne les détails du client créé.",
             response_description="Détails du client créé")
async def create_customer(customer_data: CustomerCreate, db: Session = Depends(create_db_session)):
    """
    Crée un nouveau client avec les adresses fournies.

    - **customer_data**: Données pour créer un nouveau client, incluant l'email, le nom, le téléphone, etc.
    """
    new_customer = Customer(**customer_data.model_dump(exclude={"addresses"}))
    if customer_data.addresses:
        for address_data in customer_data.addresses:
            address = Address(**address_data.model_dump())
            new_customer.addresses.append(address)

    try:
        db.add(new_customer)
        db.commit()
        db.refresh(new_customer)
    except SQLAlchemyError as e:
        db.rollback()
        handle_db_error(e)
    return new_customer


@router.put(
    "/api/customers/{customer_id}",
    response_model=CustomerResponse,
    status_code=status.HTTP_200_OK,
    summary="Mettre à Jour les Informations d'un Client",
    description="Met à jour les données téléphoniques d'un client spécifié par son ID.",
    response_description="Détails du client mis à jour")
async def update_customer(customer_id: UUID = Path(..., description="UUID unique du client à mettre à jour"),
                          update_data: CustomerUpdate = Body(..., description="Nouvelles données téléphoniques à "
                                                                              "appliquer"),
                          db: Session = Depends(create_db_session)):
    """
    Met à jour les données téléphoniques d'un client.

    - **customer_id**: UUID unique identifiant le client.
    - **update_data**: Données à mettre à jour pour le client.
    """
    updated_customer = db.query(Customer).filter(
        Customer.id == str(customer_id)).one_or_none()
    if not updated_customer:
        raise HTTPException(status_code=404, detail="Customer not found")

    updated_customer.phone = update_data.phone
    try:
        db.commit()
    except SQLAlchemyError as e:
        db.rollback()
        raise HTTPException(status_code=400, detail=str(e))
    return updated_customer


@router.get(
    "/customer/{customer_id}/addresses",
    response_model=List[AddressResponse],
    summary="Obtenir les Adresses d'un Client",
    description="Récupère et retourne toutes les adresses associées à un client spécifique.",
    response_description="Liste des adresses du client")
async def get_addresses_of_customer(customer_id: UUID, db: Session = Depends(create_db_session)):
    """
    Récupère et retourne toutes les adresses associées à un client spécifique.

    - **customer_id**: UUID unique identifiant le client.
    """
    addresses = db.query(Address).join(
        Customer_address_association,
        Customer_address_association.c.address_id == Address.id).filter(
        Customer_address_association.c.customer_id == str(customer_id)).all()

    if not addresses:
        raise HTTPException(
            status_code=status.HTTP_404_NOT_FOUND,
            detail="Aucune adresse trouvée pour ce client")

    return addresses


@router.put("/customer/{customer_id}/address/{address_id}",
            summary="Associer une Adresse à un Client",
            description="Associe une adresse existante à un client spécifié par leur ID.",
            response_description="Confirmation de l'association d'adresse")
async def associate_address_to_customer(
        customer_id: UUID = Path(..., description="UUID du client"),
        address_id: UUID = Path(..., description="UUID de l'adresse"),
        db: Session = Depends(create_db_session)):
    """
    Associe une adresse existante à un client.

    - **customer_id**: UUID du client à qui l'adresse doit être associée.
    - **address_id**: UUID de l'adresse à associer au client.
    """
    customer = db.query(Customer).filter(
        Customer.id == str(customer_id)).first()
    if not customer:
        raise HTTPException(status_code=404, detail="Client non trouvé")

    address = db.query(Address).filter(Address.id == str(address_id)).first()
    if not address:
        raise HTTPException(status_code=404, detail="Adresse non trouvée")

    try:
        # Insérer l'association dans la table CustomerAddress
        association_entry = Customer_address_association.insert().values(
            customer_id=str(customer_id),
            address_id=str(address_id)
        )
        db.execute(association_entry)
        db.commit()
    except SQLAlchemyError as e:
        db.rollback()
        raise HTTPException(status_code=400, detail=str(e))

    return {"message": "Adresse associée au client avec succès"}


@router.delete(
    "/api/customers/{customer_id}",
    status_code=status.HTTP_204_NO_CONTENT,
    summary="Supprime un Client",
    description="Supprime un client de la base de données en respectant la RGPD.")
async def soft_delete(customer_id: UUID, db: Session = Depends(create_db_session)):
    # Anonymiser les données du client
    #
    customer = db.query(Customer).filter(
        Customer.id == str(customer_id)).first()
    if not customer:
        raise HTTPException(status_code=404, detail="Client non trouvé")

    # Supprimer ou anonymiser les données personnelles
    customer.email = "deleted_{}".format(customer_id)
    customer.is_active = False
    customer.nom = "Deleted"
    customer.telephone = None

    # Marquer les adresses associées pour suppression ou anonymisation
    for address in customer.addresses:
        address.rue = None
        address.city = None
        address.zipcode = None
        address.state = None
        address.country = None

    # Dissocier les commandes du client
    db.query(Order).filter(Order.client_id ==
                           customer_id).update({'client_id': None})

    # Enregistrer la date de suppression pour la conformité RGPD
    customer.deleted_at = datetime.utcnow()

    db.commit()
    return {"message": "Client supprimé avec succès en conformité avec la RGPD"}
