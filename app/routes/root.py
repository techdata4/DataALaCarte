from fastapi import APIRouter

router = APIRouter()


@router.get("/")
async def get_homepage_message():
    return {"message": "Bienenue sur l'API DATAALACATES"}
