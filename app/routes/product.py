from datetime import datetime
from typing import List

from fastapi import APIRouter, Depends, HTTPException, status
from sqlalchemy.orm import Session

from app.dependencies.database import SessionLocal
from app.models.product import Product
from app.schemas.product_schema import ProductCreate, ProductResponse, ProductPriceResponse, ProductDetailsResponse

router = APIRouter()


def create_db_session():
    db = SessionLocal()
    try:
        yield db
    finally:
        db.close()


@router.post("/products/",
             response_model=ProductResponse,
             status_code=status.HTTP_201_CREATED,
             summary="Crée un Nouveau Produit",
             description="Crée un nouveau produit dans la base de données et retourne ses détails.")
async def create_product(product_data: ProductCreate, db: Session = Depends(create_db_session)):
    new_product = Product(**product_data.model_dump())
    db.add(new_product)
    try:
        db.commit()
        db.refresh(new_product)
    except Exception as e:
        db.rollback()
        raise HTTPException(
            status_code=status.HTTP_400_BAD_REQUEST,
            detail=str(e))
    return new_product


@router.get("/products/",
            response_model=List[ProductResponse],
            summary="Liste des Produits",
            description="Retourne une liste de tous les produits actifs.")
async def list_products(db: Session = Depends(create_db_session)):
    products = db.query(Product).filter(Product.is_active).all()
    return products


@router.get("/products/{product_id}", response_model=ProductResponse, summary="Détails d'un Produit",
            description="Retourne les détails d'un produit spécifique. Si le produit est inactif ou n'existe pas, une erreur 404 est retournée.")
async def get_product(product_id: str, db: Session = Depends(create_db_session)):
    product = db.query(Product).filter(
        Product.id == product_id,
        Product.is_active).first()
    if not product:
        raise HTTPException(
            status_code=status.HTTP_404_NOT_FOUND,
            detail="Produit non trouvé ou inactif")
    return product


@router.delete("/products/{product_id}", status_code=status.HTTP_204_NO_CONTENT, summary="Supprime un Produit",
               description="Marque un produit comme inactif et enregistre la date de suppression. Le produit n'est plus disponible mais reste dans la base de données.")
async def soft_delete_product(product_id: str, db: Session = Depends(create_db_session)):
    product = db.query(Product).filter(Product.id == product_id).first()
    if not product:
        raise HTTPException(status_code=404, detail="Produit non trouvé")
    product.is_active = False
    product.deleted_at = datetime.utcnow()
    db.commit()
    return {"message": "Produit marqué comme inactif avec succès"}
