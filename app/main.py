from fastapi import FastAPI
import json
from app.routes import root, customer, product, order, address


def create_fastapi_application():
    application = FastAPI()
    application.include_router(root.router)
    application.include_router(customer.router)
    application.include_router(product.router)
    application.include_router(order.router)
    application.include_router(address.router)

    return application


def export_openapi_schema(app, filename=' dataalacarte.json'):
    openapi_schema = app.openapi()
    with open(filename, 'w') as f:
        json.dump(openapi_schema, f, indent=2)
    print(f"OpenAPI schema has been exported to {filename}")


def run_application():
    app = create_fastapi_application()
    export_openapi_schema(app)
    import uvicorn
    uvicorn.run(app, host="0.0.0.0", port=8000)


if __name__ == "__main__":
    run_application()
