from dotenv import load_dotenv
from sqlalchemy import create_engine
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker
import os

load_dotenv()
SQLALCHEMY_SERVER_URL = os.getenv("DATABASE_URL")

# Affiche la valeur de la variable d'environnement DATABASE_URL
print(SQLALCHEMY_SERVER_URL)

engine = create_engine(
    SQLALCHEMY_SERVER_URL
)
SessionLocal = sessionmaker(autocommit=False, autoflush=False, bind=engine)

# Création de l'objet Base
Base = declarative_base()
