from typing import Optional

from pydantic import BaseModel, UUID4


class ProductBase(BaseModel):
    """
    Classe de base pour un produit.

    Attribut :
        name : Nom du produit.
        description : Description du produit.
        image :  Image du produit.
        price : Prix du produit.
        available : Disponibilité du produit (1 pour disponible, 0 pour non disponible).
    """
    name: str
    description: str
    image: str
    price: float
    available: bool


class ProductCreate(ProductBase):
    """
    Classe pour la création d'un nouveau produit.
    Hérite de ProductBase, utilisée pour créer de nouveaux produits.
    """
    id: Optional[UUID4] = None


class ProductResponse(ProductBase):
    """
    Classe pour la réponse d'un produit.

    Hérite de ProductBase, utilisée pour la réponse d'un produit.

    """

    class ProductResponse(ProductBase):
        price: Optional[float]
        name: Optional[str]
        description: Optional[str]
        available: Optional[bool] = None
        Image: str


class ProductPriceResponse(BaseModel):
    """
    Classe pour la réponse d'un produit.
        Dois contenir uniquemnt le  prix et le nom du produit; utilisée pour la réponse d'un produit.
    """
    name: Optional[str] = None
    price: float
    available: Optional[bool] = None
    image: Optional[str] = None
    description: str


class ProductDetailsResponse(ProductBase):
    """
    Classe pour la réponse d'un produit.
        Dois contenir uniquemnt la description et l'image du produit; utilisée pour la réponse d'un produit.
        L'attribut image  et description sont obligatoires. Tous les autres attributs sont optionnels.

    """
    name: Optional[str] = None
    price: Optional[float] = None
    available: Optional[bool] = None
    image: str
    description: str


class Product(ProductBase):
    """
    Classe représentant un produit complet.

    Hérite de ProductBase et ajoute un identifiant unique.
    """
    id: UUID4

    class Config:
        from_attributes = True
