from pydantic import BaseModel, UUID4, Field
from typing import Optional


class AddressBase(BaseModel) :
    """
    Classe de base pour une adresse.

    Attributs:
        street: Rue de l'adresse.
        city: Ville de l'adresse.
        state: État ou région de l'adresse.
        zipcode: Code postal de l'adresse.
        country: Pays de l'adresse.
    """
    street: str
    city: str
    state: str
    zipcode: str
    country: str


class AddressCreate(AddressBase) :
    """
    Classe pour la création d'une nouvelle adresse.
    """
    pass


class AddressResponse(AddressBase) :
    """
    Classe pour la réponse d'une adresse.
    Hérite de AddressBase et inclut l'identifiant unique de l'adresse.
    """
    id: UUID4
