from enum import Enum
from pydantic import BaseModel, UUID4
from datetime import datetime
from typing import Optional, List


class OrderStateEnum(str, Enum):
    basket = 'basket'
    validated = 'validated'
    sent = 'sent'
    delivered = 'delivered'
    cancelled = 'cancelled'


class OrderBase(BaseModel):
    customer_id: Optional[UUID4] = None
    order_date: datetime = datetime.now()
    delivery_address_id: Optional[UUID4] = None
    billing_address_id: Optional[UUID4] = None
    payment_method: Optional[str] = None
    status: OrderStateEnum = OrderStateEnum.basket


class OrderCreate(OrderBase):
    pass


class ProductInOrderLine(BaseModel):
    product_id: UUID4
    quantity: int


class OrderResponse(OrderBase):
    id: UUID4
    order_lines: List[ProductInOrderLine]


class Order(OrderBase):
    id: UUID4

    class Config:
        from_attribute = True
