from typing import Optional, List
from pydantic import BaseModel, UUID4, EmailStr
from .address_schema import AddressCreate, AddressBase


class CustomerBase(BaseModel):
    """
    Classe de base pour un client.

    Attributs:
        email: Adresse e-mail du client.
        name: Nom du client.
        phone: Numéro de téléphone du client (optionnel).
        default_address_id: Identifiant de l'adresse par défaut du client (optionnel).
        addresses: Liste des adresses associées au client.
    """
    email: Optional[EmailStr]
    name: Optional[str]
    phone: Optional[str]
    default_address_id: Optional[UUID4]
    addresses: Optional[List[AddressCreate]] = []


class CustomerCreate(CustomerBase):
    """
    Classe pour la création d'un nouveau client.

    Hérite de CustomerBase, utilisée pour créer de nouveaux clients.
    """


class CustomerUpdate(BaseModel):
    phone: str


class CustomerResponse(CustomerBase):
    """
    Classe pour la réponse d'un client.

    Hérite de CustomerBase, utilisée pour la réponse d'un client.
    """
    id: UUID4
    addresses: Optional[List[AddressBase]] = []


class Customer(CustomerBase):
    """
    Classe représentant un client.

    Hérite de CustomerBase avec l'ajout de l'identifiant unique du client et de ses adresses.
    """
    id: UUID4
    addresses: Optional[List[AddressBase]] = []

    class Config:
        from_attributes = True
