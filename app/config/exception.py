from fastapi import HTTPException
from http import HTTPStatus

# Définition des constantes pour les codes d'état HTTP
HTTP_STATUS_CUSTOMER_EXISTS = str(HTTPStatus.UNPROCESSABLE_ENTITY)
HTTP_STATUS_INTERNAL_SERVER_ERROR = str(HTTPStatus.INTERNAL_SERVER_ERROR)
HTTP_STATUS_BAD_REQUEST = str(HTTPStatus.BAD_REQUEST)
HTTP_STATUS_NOT_FOUND = str(HTTPStatus.NOT_FOUND)


def customer_exists_db_error(e: Exception) :
    """
    Détermine si une erreur de base de données indique qu'un client existe déjà.

    :param e: L'erreur de base de données sous forme d'Exception.
    :return: True si le client existe déjà, False sinon.
    """


def internal_server_db_error(e: Exception) :
    """
    Vérifie si une exception donnée représente une erreur interne du serveur de base de données.

    :param e: Exception survenue lors de l'opération de base de données.
    :return: True si l'exception représente une erreur interne de serveur de base de données, False sinon.
    """


def bad_request_db_error(e: Exception) :
    """
    Vérifie si l'exception donnée est une mauvaise requête liée à une erreur de base de données.

    :param e: Objet Exception à vérifier
    :return: True si l'exception est une mauvaise requête liée à une erreur de base de données, False sinon
    """


def raise_http_exception(e: Exception, status: int) :
    """
    Lève une HTTPException avec le code d'état donné et le détail de l'exception fournie.

    :param e: L'exception à lever comme une HTTPException.
    :type e: Exception
    :param status: Le code d'état HTTP à définir dans l'HTTPException.
    :type status: int
    :return: None
    """


def create_error_status_map() :
    """
    Crée une carte de statut d'erreur.

    :create_error_status_map() :

    Return :
        Un dictionnaire qui associe des codes d'erreur spécifiques à des codes d'état HTTP correspondants.
        Les codes d'erreur sont liés aux erreurs de base de données, et les codes d'état HTTP correspondants
        sont déterminés en fonction de la gravité de l'erreur.

    Exemple :
        >>> create_error_status_map()
        {
            customer_exists_db_error : HTTPStatus.UNPROCESSABLE_ENTITY,
            internal_server_db_error : HTTPStatus.INTERNAL_SERVER_ERROR,
        }
    """


def handle_db_error(e: Exception) :
    """
    Gère l'erreur de base de données en vérifiant la carte de statut d'erreur et en levant les exceptions HTTP appropriées.

    :param e: L'exception soulevée lors de l'opération de base de données.
    :type e: Exception
    :return: None
    """
